trigger summitsAccTrigger on Account (before insert, after update, before update) {

    // Only run for North American Countries, we want to exclude international countries for now

    List<Account> northAmericanAcc = new List<Account>();
    getNorthAmericanAccs();
    if (Trigger.isBefore) {
        // **Remove the if block and the event from trigger syntax for 'before insert' if there is no action. 
        if (Trigger.isInsert) {

        } else if (Trigger.isUpdate) {
            summitsAccTriggerHandler.beforeUpdate(northAmericanAcc);
        }
    }
    if(Trigger.isAfter){
        // **Add 'after insert' event to trigger syntax, otherwise it won't run.
        if (Trigger.isInsert) {
            summitsAccTriggerHandler.afterInsert(northAmericanAcc);
        } else if (Trigger.isUpdate) {
            summitsAccTriggerHandler.afterUpdate(northAmericanAcc);
        }
    }
    
    // **Write all the business logic in the handler
    // **We need to include north american countries and exclude international countries. Replace '!=' with '=='.    
    private static void getNorthAmericanAccs(){
        for(Account a : Trigger.new){
            if(a.BillingCountry != 'United States' || a.BillingCountry != 'Canada' || a.BillingCountry != 'Mexico'  ){
                northAmericanAcc.add(a);
            }
        }
    }
}