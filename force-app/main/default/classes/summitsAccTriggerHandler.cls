public class summitsAccTriggerHandler {
    // **Add try catch blocks for exception handling
    
    // Account Partner Record Type
    // ** Record type Ids should not be hard-coded in the code. IDs are different in different orgs. Get the Record Type ID dynamically from the record type name.
    public static String partnerRecordType = '01221000001PIifAAG';
    // ** Store credentials in Named Credentials. Do not hard-code and expose credentials.
    public static final String username = 'thisIsMyUsername@test.com';
    public static final String password = 'passWord123';
    
    public static void afterUpdate(List<Account> newAccounts) {
        alterContactsCountry(newAccounts);
        RectifyOpportunities(newAccounts);
    }

    public static void beforeUpdate(List<Account> newAccounts) {
        checkWebsite(newAccounts);
    }

    public static void afterInsert(List<Account> newAccounts) {
        sendAccountToERP(newAccounts);
    }
    
    // Every Contact related to an account should have the same mailing country as its parent accounts billing country, if they do not match, update it, do SOQL in loop
    // ** No SOQL in for loop. It will throw governor limit error.
    private static void alterContactsCountry(List<Account> newAccounts){
        List<Contact> contactUpdateList = new List<Contact>();
        for(Account a: newAccounts){
            contactUpdateList.addAll(getMismatchContacts(a, 'MailingCountry', 'BillingCountry'));
        }
        // **Check if list is not null, then update. 
        update contactUpdateList;
    }
    
    // Any account with the record type of partner should have a website, if not add an error for the end user    
    public static void checkWebsite(List<Account> accList){
         for(Account a: accList)
             // **Use this syntax to get record type id by name - Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId()
             // **Replace RecordType.Id with RecordTypeId
             if(a.RecordType.Id == partnerRecordType && a.Website == null) 
                 a.addError('This Account is a partner account and thus needs a website!');
    }

    public static List<Contact> getMismatchContacts(Account singleAccount, String firstValue, String secondValue){
		// **System.QueryException: Only variable references are allowed in dynamic SOQL/SOSL. ':' should not be used when using a query string.  
		// **No SOQL in for loop    
        String qryString = 'SELECT Id, Name, MailingCountry FROM Contact WHERE ' + firstValue + '!=: +singleAccount.' + secondValue;
        List<Contact> tempContacts = Database.query(qryString);
        for(Contact a: tempContacts){
            a.MailingCountry = singleAccount.BillingCountry; // **Add each contact 'a' to a list and return that list.
        }
        return tempContacts;
    }

    // Sum up the $ amount of all opportunities for each account and override the annual revenue field on the account
    // **Recursive trigger. Handle recursion by making sure trigger runs only once.
    public static void RectifyOpportunities(List<Account> newAccounts){
        Map<Id, Decimal> accOpportunitiesMap = new Map<Id, Decimal>();
        List<Account> updateAccRevenue = new List<Account>();
        for(Opportunity a : [SELECT Id, AccountId, Amount FROM Opportunity WHERE AccountId IN: newAccounts]){
            Decimal oppValue = 0;
            // **Replace Account.Id with Account Id
            if(accOpportunitiesMap.get(a.Account.Id) == null){
                accOpportunitiesMap.put(a.AccountId, a.Amount);
            }
            else{
                // **Replace Account.Id with Account Id
                oppValue = accOpportunitiesMap.get(a.Account.Id);
                // **System.NullPointerException: Attempt to de-reference a null object. oppValue
                accOpportunitiesMap.put(a.AccountId, a.Amount + oppValue);
            }
        }
        for(Id a : accOpportunitiesMap.keySet()){
            Account acc = new Account(Id = a);// **Don't create a new account. Update the existing account.
            acc.AnnualRevenue = accOpportunitiesMap.get(a);
            updateAccRevenue.add(acc);
        }
        try {
            system.debug('here');
            // **Check if the list is not null, then update. Update 'acc' and not 'updateAccRevenue'.
            update updateAccRevenue;
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
    }
    

    // post all account to the external ERP system for billing purposes
    // **System.CalloutException: Callout from triggers are currently not supported. Use 'future(callout=true)' or queueable apex in case of multiple API calls.    
    private static void sendAccountToERP(List<Account> accounts){
        for(Account a :accounts){
            HttpResponse res = postAccount(a);
            System.debug('The res ' + res);
        }
    }

    @TestVisible
    private static HttpResponse postAccount(Account acc){
        String payload = JSON.serialize(acc);
        // **Use Named credentials to securely store the authentication
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', authorizationHeader);
        // **Use Named credentials to store the endpoint
        req.setEndpoint('http://www.randomWebsite.com/');
        req.setMethod('POST');
        req.setBody(payload);
        HttpResponse res = h.send(req);
        return res;
    }
}