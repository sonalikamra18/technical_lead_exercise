@IsTest
// **Private class
global class summitsAccTriggerHandlerTest {
    
    @TestSetup
    static void testSetup(){
        Integer count = 100;
        List<Account> accList = new List<Account>();
        for(Integer i = 0 ; i < 100 ; i++){
            Account temp = new Account(Name = 'Test Acc ' + i);
            accList.add(temp);
        }
        insert accList;

        List<Contact> contactList = new List<Contact>();
        for(Integer i = 0 ; i < accList.size() ; i++){
            Contact temp = new Contact(LastName = 'Test Contact ' + i, AccountId = accList[i].Id, Contact_Type__c = 'Host-Presenter');
            contactList.add(temp);
        }
        insert contactList;
    }

    @IsTest
    static void testWebsite(){
        List<Account> accList = [SELECT Id, Website, RecordTypeId FROM Account];
        for(Account a : accList){
            a.RecordTypeId = summitsAccTriggerHandler.partnerRecordType;
        }
        // ** Do not use try catch here
        try {
            // **Add each Account 'a' to a new list and then update that list
            update accList;
        }catch (Exception e){
            System.debug(System.LoggingLevel.FINER, 'Exception: ' + e);
            //Assert will pass
            System.assert(e.getMessage().contains('This Account is a partner account and thus needs a website!'));
        }
    }
    @IsTest
    static void testMatchingAddresses(){
        List<Account> accList = [SELECT Id, Website, RecordTypeId, BillingCountry FROM Account];
        List<Contact> contactList = [SELECT Id, MailingCountry FROM Contact];

        //Assert will pass
        for(Contact a : contactList){
            // **Incorrect use of System.assert
            System.assert(a.MailingCountry == null);
        }
        for(Account a : accList){
            a.BillingCountry = 'United States';
        }
        Test.startTest();
         	// **Do not update the list in test.startTest() method
        	// **Add each Account 'a' to a new list and then update that list
            update accList;
        Test.stopTest();
        List<Contact> requeryContacts = [SELECT Id, MailingCountry FROM Contact];

        //Assert will pass
        for(Contact a : requeryContacts){
            // **Incorrect use of System.assert
            System.assert(a.MailingCountry != null);
        }
    }
    @IsTest
    // **Methods defined as TestMethod do not support Web service callouts. Need a mock http class to test callouts.
    static void testERPmethod(){
        List<Account> accList = [SELECT Id, Website, RecordTypeId, BillingCountry FROM Account];

        Test.startTest();
            summitsAccTriggerHandler.postAccount(accList[0]);
        Test.stopTest();
    }
}